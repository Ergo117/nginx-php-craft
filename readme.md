# Nginx Php Craft

This repo is responsible for building 2 images next to each other

1. `registry.gitlab.com/bowendev/nginx-php-craft:php74_<version>`
2. `registry.gitlab.com/bowendev/nginx-php-craft:php80_<version>`

For simplicity they are versioned alongside each other despite being different versions of php. No attempt should be made to version new images separately. Even if only one of the two is updated, we are going to accept the other unchanged image will have a new version and still be unchanged.

## !! Legacy Image

This is a legacy image that is kept around for a few items that had a hard time converting over to the newer craft-owned dockerhub images. It should NOT be used for newer projects in general.
